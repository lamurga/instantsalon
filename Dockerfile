FROM python:3.6

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev

COPY ./requirements.txt /code/requirements.txt
RUN pip install -r /code/requirements.txt
RUN pip install gunicorn

COPY . /code/
WORKDIR /code/

RUN python manage.py migrate

RUN useradd instantsalon
RUN chown -R wagtail /code
USER instantsalon

EXPOSE 8000
CMD exec gunicorn instantsalon.wsgi:application --bind 0.0.0.0:8000 --workers 3
