from django.contrib.auth.models import User
from django.db import models


'''
    MODULO DE USUARIO
    
    Se extiende del model user de django. Para generar un perfil y otros procesos.
'''


class Profile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE
    )
    birthdate = models.DateField()
