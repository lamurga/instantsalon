# -*- coding: utf-8 -*-
from django.db import migrations


def create_homepage(apps, schema_editor):
    ContentType = apps.get_model('contenttypes.ContentType')
    Page = apps.get_model('wagtailcore.Page')
    Site = apps.get_model('wagtailcore.Site')
    HomePage = apps.get_model('base.HomePage')

    Page.objects.filter(id=2).delete()

    homepage_content_type, __ = ContentType.objects.get_or_create(
        model='homepage', app_label='base')

    homepage = HomePage.objects.create(
        title="InstantSalon",
        draft_title="InstantSalon",
        slug='home',
        content_type=homepage_content_type,
        path='00010001',
        depth=2,
        numchild=0,
        url_path='/home/',
    )

    Site.objects.create(
        hostname='localhost', root_page=homepage, is_default_site=True)


def remove_homepage(apps, schema_editor):
    ContentType = apps.get_model('contenttypes.ContentType')
    HomePage = apps.get_model('base.HomePage')

    HomePage.objects.filter(slug='home', depth=2).delete()
    ContentType.objects.filter(model='homepage', app_label='base').delete()


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_homepage, remove_homepage),
    ]
