from wagtail.core import hooks


@hooks.register('construct_main_menu')
def hide_snippets_menu_item(request, menu_items):
    lst_to_delete = ['media', 'images', 'documents']
    menu_items[:] = [item for item in menu_items if item.name not in lst_to_delete]
