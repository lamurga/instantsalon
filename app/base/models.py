from django.utils.translation import ugettext as _
from django.db import models
from wagtail.core.models import Page
from wagtail.snippets.models import register_snippet


'''
    MODULO BASE:
    
    Se considera este modulo para todo objeto que no se incluya en otros modulos. Y se utilice como el core de la app. 
    
'''


@register_snippet
class Service(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(decimal_places=2, max_digits=6)
    duration = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = _("Services")


class HomePage(Page):

    @property
    def get_services(self):
        return Service.objects.all()
