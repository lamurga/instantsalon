from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, StreamFieldPanel
from wagtail.core.models import Orderable, Page
from django.conf import settings
from modelcluster.fields import ParentalKey
from datetime import datetime
from app.base.models import Service


'''
    MODULO DE SALON:
    
    La idea sería que el salon sea el detalle final, donde se incluya los servicios.
    Además, se tomaría como sucursales. Donde se especificaría su ubicación. Y configuracion de horarios disponibles.
    Se podria extender con google maps con las cordenadas para una mejor experiencia.

'''


class OperatingHours(models.Model):
    DAY_CHOICES = (
        ('MON', 'Monday'),
        ('TUES', 'Tuesday'),
        ('WED', 'Wednesday'),
        ('THUR', 'Thursday'),
        ('FRI', 'Friday'),
        ('SAT', 'Saturday'),
        ('SUN', 'Sunday'),
    )

    day = models.CharField(
        max_length=4,
        choices=DAY_CHOICES,
        default='MON'
    )
    opening_time = models.TimeField(
        blank=True,
        null=True
    )
    closing_time = models.TimeField(
        blank=True,
        null=True
    )
    closed = models.BooleanField(
        "Closed?",
        blank=True,
    )

    panels = [
        FieldPanel('day'),
        FieldPanel('opening_time'),
        FieldPanel('closing_time'),
        FieldPanel('closed'),
    ]

    class Meta:
        abstract = True

    def __str__(self):
        if self.opening_time:
            opening = self.opening_time.strftime('%H:%M')
        else:
            opening = '--'
        if self.closing_time:
            closed = self.opening_time.strftime('%H:%M')
        else:
            closed = '--'
        return '{}: {} - {} {}'.format(
            self.day,
            opening,
            closed,
            settings.TIME_ZONE
        )


class SalonOperatingHours(Orderable, OperatingHours):
    salon = ParentalKey(
        'SalonPage',
        related_name='hours_of_operation',
        on_delete=models.CASCADE
    )


class SalonPage(Page):
    description = models.TextField(
        help_text='Text to describe the salon',
        blank=True)
    address = models.TextField()
    service = models.ForeignKey(
        Service,
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    content_panels = [
        FieldPanel('title', classname="full"),
        FieldPanel('description', classname="full"),
        FieldPanel('address', classname="full"),
        InlinePanel('hours_of_operation', label="Hours of Operation"),
    ]

    def __str__(self):
        return self.title

    @property
    def operating_hours(self):
        hours = self.hours_of_operation.all()
        return hours

    # determino si el salon esta abierto
    def is_open(self):
        now = datetime.now()
        current_time = now.time()
        current_day = now.strftime('%a').upper()
        try:
            self.operating_hours.get(
                day=current_day,
                opening_time__lte=current_time,
                closing_time__gte=current_time
            )
            return True
        except SalonOperatingHours.DoesNotExist:
            return False
